from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter

def create_pdf(introduction, fonctionnalites, programmes):
    pdf_filename = "projet_admission.pdf"
    pdf_canvas = canvas.Canvas(pdf_filename, pagesize=letter)

    # Définir la police par défaut
    pdf_canvas.setFont("Helvetica", 12)

    # Ajouter le titre
    pdf_canvas.drawCentredString(300, 750, "Projet d'Introduction - Module d'Admission")

    # Ajouter l'introduction
    pdf_canvas.setFont("Helvetica", 10)
    pdf_canvas.drawString(50, 730, "Introduction:")
    text_object = pdf_canvas.beginText(50, 710)
    text_object.setFont("Helvetica", 10)
    text_object.setTextOrigin(50, 710)
    text_object.textLines(introduction)
    pdf_canvas.drawText(text_object)

    # Ajouter les fonctionnalités
    pdf_canvas.drawString(50, 660, "Fonctionnalités:")
    for i, fonctionnalite in enumerate(fonctionnalites, start=1):
        pdf_canvas.drawString(70, 640 - i * 15, f"{i}. {fonctionnalite}")

    # Ajouter les programmes
    pdf_canvas.drawString(50, 545, "Programmes et Trimestres:")
    for i, programme in enumerate(programmes, start=1):
        pdf_canvas.drawString(70, 540 - i * 15, f"{i}. {programme}")

    # Sauvegarder le fichier PDF
    pdf_canvas.save()

    print(f"Le fichier PDF '{pdf_filename}' a été créé avec succès.")



introduction = (
    "Le module d’admission est le premier point de contact entre les étudiants et l’institution. "
    "Il permet aux étudiants de soumettre des demandes d’admission en ligne pour les programmes de leur choix, "
    "de manière simple et rapide."
)

fonctionnalites = [
    "La création d’un compte personnel, qui permet aux étudiants de s’identifier et de gérer leurs informations personnelles, académiques et professionnelles.",
    "Le remplissage du formulaire d’admission",
    "La vérification des pièces justificatives",
    "La consultation du statut de la demande",
    "La confirmation de l’acceptation"
]

programmes = [
    "Programme 1 - Génie logiciel (Baccalauréat, Cycle 1)",
    "Programme 2 - Développement web (Master, Cycle 2)",
    "Programme 3 - Analyse de données en biologie (Doctorat, Cycle 3)"
]

# def generate_response(user_input):
#     completions = openai.Completion.create(
#         engine='text-davinci-003',
#         prompt=user_input,
#         max_tokens = 1024,
#         n=1, 
#         stop = None,
#         temperature = 0.5,
#     )
#     message = completions.choices[0].text.strip()
#     return message