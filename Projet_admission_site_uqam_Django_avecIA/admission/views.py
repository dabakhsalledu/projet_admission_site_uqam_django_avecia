from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings import OpenAIEmbeddings
from langchain.chains.question_answering import load_qa_chain
from langchain.vectorstores import FAISS
from PyPDF2 import PdfReader
import os
from langchain.chat_models import ChatOpenAI
from langchain.memory import ConversationBufferMemory
from langchain.chains import ConversationalRetrievalChain

# from  .test import create_pdf, introduction,fonctionnalites,programmes
 
# pip install openai streamlit streamlit-chat
import openai
os.environ['OPENAI_API_KEY'] = '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'
# openai.api_key = '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'
chemin='C:/Users/henri/Documents/cour_documentation/Projet_admission/DissertationFinale.pdf'
import os
 
def read_pdf_from_project():
    project_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # Chemin absolu du répertoire du projet
    pdf_path = os.path.join(project_directory, 'pdf_files', r'C:\Users\henri\Documents\cour_documentation\Projet_admission\DissertationFinale.pdf')  # Chemin absolu du fichier PDF
   
    # Lecture du contenu du fichier PDF
    with open(pdf_path, 'rb') as pdf_file:
        pdf_content = pdf_file.read()
   
    return pdf_content
 
def generate_response(user_input):
    completions = openai.Completion.create(
        engine='text-davinci-003',
        prompt=user_input,
        max_tokens = 1024,
        n=1,
        stop = None,
        temperature = 0.5,
    )
    message = completions.choices[0].text.strip()
    return message
 
# Get pdf, extract text, divide chunks
def get_extract_chunks(pdf_path):
    content = ""
    try:
        with open(pdf_path, 'rb') as pdf_file:
            reader = PdfReader(pdf_file)
            for page in reader.pages:
                content += page.extract_text()
    except Exception as e:
        # Gérer les erreurs lors de l'extraction du contenu PDF
        pass
   
    splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1400,
        chunk_overlap=320,
        length_function=len
    )
    chunks = splitter.split_text(content)
    return chunks
 
def create_vectorstore(chunks):
    current_embedding = OpenAIEmbeddings()
    vectorestore = FAISS.from_texts(texts=chunks, embedding=current_embedding)
    return vectorestore
     
def get_conversation_chain(vectorestore):
    current_llm = ChatOpenAI()
    current_memory = ConversationBufferMemory(
        memory_key='chat_history', return_messages=True
    )
    conversation_chain = ConversationalRetrievalChain.from_llm(
        llm=current_llm,
        retriever=vectorestore.as_retriever(),
        memory=current_memory
    )
    return conversation_chain
def index_view(request):
    if request.method=='POST':
        getmessage=request.POST.get('message','input')
        pdf_path = r'C:\Users\KASSAMBARA ABRAMANE\source\repos\My_Projet\Projet_Admission\prj_Admission_DT\Projet_admission\projet_admission.pdf'
        message=get_extract_chunks(pdf_path)
        vector=create_vectorstore(message)
        docs = vector.similarity_search(getmessage)
        llm =ChatOpenAI()
        chain =load_qa_chain(llm, chain_type= 'stuff' )
        reponse = chain.run(input_documents=docs, question=getmessage)
        print(reponse)
        return JsonResponse({'message':getmessage,'response':reponse})
    # Autres instructions ou rendu de la page dans d'autres cas...
    return render(request, 'index.html')
def programme_view(request):

    return render(request, 'programme.html')


def admission_view(request):
    return render(request, 'admissions.html')


def propos_view(request):
    return render(request, 'propos.html')


def contact_view(request):
    return render(request, 'contact.html')
