from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import dashboard, traite_demande,login,send_approval_email,refuse_view
urlpatterns = [
    path('admin_admission/',login,name='login'),
    path('dashboard/', dashboard, name='dashboard'),
    path('send_approval_email/', send_approval_email, name='send_approval_email'),
    path('demande/<int:demande_id>/', traite_demande, name='traite_demande'),
    path('refuser/', refuse_view, name='refuse_view'),

]
# Ajouter ces lignes à la fin du fichier pour servir les fichiers médias en mode développement
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)