
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import pandas as pd
def train_model(texte_releve_note,texte_atesttation,texte_acte_naissance,texte_lettre_motivation,piece_identite):
    data = {
        'Texte': [texte_releve_note, texte_atesttation, texte_acte_naissance, texte_lettre_motivation,piece_identite],
        'Question': ['Quelle est votre opinion sur ce document ?', 'Votre avis sur ce texte ?', 'Que pensez-vous de ce document ?', 'Quelle est votre impression sur ce texte ?'],
        'Admissibilite': [1, 0, 1, 0]
        # 1 pour admissible, 0 pour non admissible  
    }
    df = pd.DataFrame(data)
    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(df['Texte'] + ' ' + df['Question'])
    y = df['Admissibilite']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    model = SVC(kernel='linear')
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print(f'Précision du modèle : {accuracy}')
    return vectorizer, model
