from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import smtplib
from django.shortcuts import get_object_or_404, redirect, render
from .models import Admin_Admission
from demande_admission.models import Demander_Admission,LettreAdmission
def dashboard(request):
    demandes = Demander_Admission.objects.all()
    username = request.session['username']
    context = {'demandes': demandes,
               'username': username}
    return render(request, 'admins/dashboard.html', context)
def login(request):
    if request.method == "POST":
        username =  request.POST.get('username')
        password = request.POST.get('password')
        user,succ = Admin_Admission.login_user(username=username,password=password)
        if succ:
            request.session['username'] = username
            return redirect('dashboard')
        else :
            context = {'message':'Username or Password is incorrect!'}
            return redirect('login',context=context)
    return render(request, 'admins/login.html')
import cv2
import pytesseract
from PIL import Image
from PyPDF2 import PdfFileWriter, PdfFileReader
from textblob import TextBlob
def extract_text_from_image(image_path):
    try:
        image = cv2.imread(str(image_path))
        if image is None:
            raise Exception(f"Impossible de lire l'image : {image_path}")
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        resized_image = cv2.resize(gray_image, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
        text = pytesseract.image_to_string(resized_image)
        text = pytesseract.image_to_string(gray_image)
        if text.strip() == "":
            raise Exception(f"Tesseract n'a pas pu extraire de texte de l'image : {image_path}")
        return text
    except Exception as e:
        print(f"Erreur lors de l'extraction du texte de l'image : {e}")
        return None
import re
from PyPDF2 import PdfReader
from langchain.text_splitter import CharacterTextSplitter
def extract_information(text, keyword):
    pattern = re.compile(f'{keyword} : (.+?)(?:\\n|$)', re.IGNORECASE)
    match = pattern.search(text)
    if match:
        return match.group(1).strip()
    else:
        return ""
import spacy
def find_keywords_with_spacy(keywords, text):
    found_keywords = set()
    nlp = spacy.load("fr_core_news_sm")
    doc = nlp(text)
    for ent in doc.ents:
        if ent.text.lower() in keywords:
            found_keywords.add(ent.text)

    return found_keywords
def traite_demande(request, demande_id):
    demande = get_object_or_404(Demander_Admission, id=demande_id)
    request.session['demande_id'] = demande_id
    if request.method == 'POST':
        action = request.POST.get('action', '')
        if action == 'lire_avec_ia':
            pytesseract.pytesseract.tesseract_cmd = r'C:\Users\KASSAMBARA ABRAMANE\source\repos\My_Projet\Projet_Admission\prj_Admission_DT\Tesseract-OCR\tesseract.exe'
            documents = demande.Document_Fournir
            releve_note = documents.releve_note
            atesttation = documents.attestation
            acte_naissance = documents.acte_Naissance
            lettre_motivation = documents.lettre_motivation
            piece_identite = documents.piece_identite
            texte_releve_note = extract_text_from_image(releve_note)
            texte_atesttation = extract_text_from_image(atesttation)
            texte_acte_naissance = extract_text_from_image(acte_naissance)
            texte_lettre_motivation = extract_text_from_image(lettre_motivation)
            texte_piece_identite = extract_text_from_image(piece_identite)
            print (f"\nText Relev : {texte_releve_note}")
            print (f"\nTexte Atesttation : {texte_atesttation}")
            print (f"\nTexte Acte Naissance : {texte_acte_naissance}")
            print (f"\nTexte Lettre Motivation : {texte_lettre_motivation}")
            print (f"\nTexte Piece Identité : {texte_piece_identite}")
            # Traitement des données pour le relevé de note

            
        elif action == 'accepter':
            pass
        elif action == 'refuser':
            context = {
                'raison':True,
                'demande': demande
            }
            if request.method == 'POST':
                action = request.POST.get('action', '')
                raison =  request.POST.get('raison_')
                print (f"Voila les Raison {raison}")
                return render(request, 'admins/traite_demande.html',context=context)
    return render(request, 'admins/traite_demande.html', {'demande': demande})
from django.http import JsonResponse
import random
import string
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from io import BytesIO
from email.mime.text import MIMEText
from django.core.files.base import ContentFile
from django.http import JsonResponse
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import random
import smtplib
import string
from django.shortcuts import get_object_or_404
from django.core.files.base import ContentFile
from reportlab.pdfgen import canvas
from io import BytesIO
# Autres importations...
# def generate_admission_letter(demande):
#     getnom = demande.Information_Personnel.nom
#     getprenom = demande.Information_Personnel.prenom
#     # Générez le code permanent
#     random_numbers = ''.join(random.choices(string.digits, k=5))
#     segmentsnom = [getnom[i:i + 3] for i in range(0, len(getnom), 3)]
#     segmentsprenom = getprenom[0]
#     variable_1 = segmentsnom[0]
#     code_permanent = variable_1 + segmentsprenom + random_numbers
#     # Votre contenu HTML pour la lettre d'admission
#     contenu_lettre = f"""
#         <html>
#             <!-- Votre style CSS ici -->
#             <body>
#                 <div class="container">
#                     <h2>Confirmation de la demande d'admission en ligne à l'UQAM</h2>
#                     <p>Bonjour {demande.Information_Personnel.prenom} {demande.Information_Personnel.nom},</p>
#                     <p>Nous avons le plaisir de vous informer que votre demande d'admission à l'UQAM a été acceptée.</p>
#                     <ul>
#                         <li>Frais de scolarité : {demande.ProgrammeTrimestre.programme.prix_Programme}$</li>
#                         <li>Code permanent : {code_permanent}</li>
#                         <li>Programme : {demande.ProgrammeTrimestre.programme}</li>
#                         <li>Session : {demande.ProgrammeTrimestre.trimestre}</li>
#                     </ul>
#                     <!-- Informations complémentaires -->
#                     <div class="contact-info">
#                         <h3>Informations complémentaires :</h3>
#                         <p>Pour toute question ou assistance, veuillez contacter le Service d'Admission :</p>
#                         <p>Email : anonymousteccart@gmail.com</p>
#                         <p>Téléphone : +1 514-456-7890</p>
#                     </div>
#                 </div>
#             </body>
#         </html>
#     """
#     # Générer le PDF avec ReportLab
#     pdf_buffer = BytesIO()
#     pdf_canvas = canvas.Canvas(pdf_buffer)
#     pdf_canvas.drawString(100, 100, "Hello World")  # Remplacez cela par le contenu réel du PDF
#     pdf_canvas.save()
#     pdf_data = pdf_buffer.getvalue()
#     pdf_buffer.close()
#     return contenu_lettre, code_permanent, pdf_data
def generate_admission_letter(demande):
    getnom = demande.Information_Personnel.nom
    getprenom = demande.Information_Personnel.prenom
    # Générez le code permanent
    random_numbers = ''.join(random.choices(string.digits, k=5))
    segmentsnom = [getnom[i:i + 3] for i in range(0, len(getnom), 3)]
    segmentsprenom = getprenom[0]
    variable_1 = segmentsnom[0]
    code_permanent = variable_1 + segmentsprenom + random_numbers
    # Obtenez d'autres informations nécessaires
    programme = demande.ProgrammeTrimestre.programme.titre
    prix_programme = demande.ProgrammeTrimestre.programme.prix_Programme
    trimestre = demande.ProgrammeTrimestre.trimestre
    # Générer le PDF avec ReportLab
    pdf_buffer = BytesIO()
    pdf_canvas = canvas.Canvas(pdf_buffer, pagesize=letter)
    pdf_canvas.setFont("Helvetica", 12)

    # Ajouter du texte au canvas avec un titre et une couleur différente
    pdf_canvas.setFont("Helvetica-Bold", 16)  # Police en gras et taille 16
    pdf_canvas.setFillColorRGB(0, 0, 0)  # Couleur du texte en RVB (0, 0, 0) pour noir
    pdf_canvas.drawString(100, 760, "Lettre d'Admission UQAM")  # Titre
    pdf_canvas.setFont("Helvetica", 12)  # Revenir à la police par défaut et taille 12
    pdf_canvas.setFillColorRGB(0, 0, 0)  # Revenir à la couleur par défaut (noir)

    pdf_canvas.drawString(100, 730, f" Bonjour {getprenom} {getnom},")
    pdf_canvas.drawString(100, 710, f"Nous avons le plaisir de vous informer que votre demande \nd'admission à l'UQAM a été acceptée.")
    pdf_canvas.drawString(100, 690, f"Code permanent : {code_permanent}")
    pdf_canvas.drawString(100, 670, f"Programme : {programme}")
    pdf_canvas.drawString(100, 650, f"Prix du programme : {prix_programme}$")
    pdf_canvas.drawString(100, 630, f"Trimestre : {trimestre}")
    # ... Ajoutez d'autres éléments sur le canvas en fonction de vos besoins
    pdf_canvas.save()
    pdf_data = pdf_buffer.getvalue()
    pdf_buffer.close()
    return code_permanent, pdf_data

def save_pdf_to_model(demande, contenu_lettre):
    code_permanent, pdf_data = generate_admission_letter(demande)
    lettre_admission = LettreAdmission(
        demandeur=demande.demandeur,
        contenu=contenu_lettre,
    )
    lettre_admission.save()
    lettre_admission.contenu_pdf.save(f"lettre_admission_{demande.id}.pdf", ContentFile(pdf_data))
    return pdf_data

def send_admission_email(demande, contenu_lettre, pdf_data):
    smtp_server = 'smtp.gmail.com'
    smtp_port = 587
    sender_email = 'anonymousteccart@gmail.com'
    sender_password = 'ddnh pgpe yape wchg'
    message = MIMEMultipart()
    message["Subject"] = "Confirmation de la demande d'admission à l'UQAM"
    message["From"] = sender_email
    message["To"] = demande.demandeur.email
    # Corps du message
    body = MIMEText(contenu_lettre, "html")
    message.attach(body)
    # Pièce jointe PDF
    pdf_attachment = MIMEApplication(pdf_data, _subtype="pdf")
    pdf_attachment.add_header('content-disposition', 'attachment', filename=f"lettre_admission_{demande.id}.pdf")
    message.attach(pdf_attachment)
    with smtplib.SMTP(smtp_server, smtp_port) as server:
        server.starttls()
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, demande.demandeur.email, message.as_string())

def send_approval_email(request):
    demande_id = request.session.get('demande_id')
    demande = get_object_or_404(Demander_Admission, id=demande_id)
    demande.statut_demande = "Accepté"
    demande.save()
    contenu_lettre = "Contenu de la lettre"  # Remplacez cela par le contenu réel de votre lettre d'admission
    pdf_data = save_pdf_to_model(demande, contenu_lettre)
    send_admission_email(demande, contenu_lettre, pdf_data)
    return JsonResponse({'message': 'Envoyé avec succès !'})


def refuse_view(request):
    reason = request.POST.get('reason')
    print (reason)
    return JsonResponse({'message': 'Envoyé avec succès !'})


