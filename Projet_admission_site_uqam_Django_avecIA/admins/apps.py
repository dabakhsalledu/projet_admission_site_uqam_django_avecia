from django.apps import AppConfig


class GererAdmissionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'admins'
